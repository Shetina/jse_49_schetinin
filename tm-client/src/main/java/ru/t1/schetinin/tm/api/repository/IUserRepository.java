package ru.t1.schetinin.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.schetinin.tm.dto.model.UserDTO;

public interface IUserRepository extends IRepository<UserDTO> {

    @Nullable
    UserDTO findByLogin(@Nullable String login);

    @Nullable
    UserDTO findByEmail(@Nullable String email);

    Boolean isLoginExist(@NotNull String login);

    Boolean isEmailExist(@NotNull String email);

}
