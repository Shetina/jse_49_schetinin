package ru.t1.schetinin.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import ru.t1.schetinin.tm.dto.model.ProjectDTO;

public interface IProjectRepository extends IUserOwnedRepository<ProjectDTO> {

    @NotNull
    ProjectDTO create(@NotNull String userId, @NotNull String name, @NotNull String description);

    @NotNull
    ProjectDTO create(@NotNull String userId, @NotNull String name);

}