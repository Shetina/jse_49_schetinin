package ru.t1.schetinin.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.schetinin.tm.api.endpoint.IAuthEndpoint;
import ru.t1.schetinin.tm.api.endpoint.IUserEndpoint;
import ru.t1.schetinin.tm.api.service.IPropertyService;
import ru.t1.schetinin.tm.dto.request.*;
import ru.t1.schetinin.tm.marker.SoapCategory;
import ru.t1.schetinin.tm.dto.model.UserDTO;
import ru.t1.schetinin.tm.service.PropertyService;

@Category(SoapCategory.class)
public class AuthEndpointTest {

    @NotNull
    private static final IPropertyService PROPERTY_SERVICE = new PropertyService();

    @NotNull
    private static final IAuthEndpoint AUTH_ENDPOINT = IAuthEndpoint.newInstance(PROPERTY_SERVICE);

    @NotNull
    private static final IUserEndpoint USER_ENDPOINT = IUserEndpoint.newInstance(PROPERTY_SERVICE);

    @Nullable
    private static String adminToken;

    @BeforeClass
    public static void setUp() {
        @NotNull final UserLoginRequest loginRequest = new UserLoginRequest();
        loginRequest.setLogin("admin");
        loginRequest.setPassword("admin");
        adminToken = AUTH_ENDPOINT.login(loginRequest).getToken();
        @NotNull final UserRegistryRequest request = new UserRegistryRequest(adminToken);
        request.setLogin("userTestLogin");
        request.setPassword("userTestPassword");
        request.setEmail("testEmail");
        USER_ENDPOINT.registryUser(request);
    }

    @AfterClass
    public static void tearDown() {
        @NotNull final UserRemoveRequest request = new UserRemoveRequest(adminToken);
        request.setLogin("userTestLogin");
        USER_ENDPOINT.removeUser(request);
    }

    private String getUserToken() {
        @NotNull final UserLoginRequest request = new UserLoginRequest();
        request.setLogin("userTestLogin");
        request.setPassword("userTestPassword");
        return AUTH_ENDPOINT.login(request).getToken();
    }

    @Test
    public void loginUser() {
        @Nullable final String token = getUserToken();
        Assert.assertNotNull(token);
        @NotNull final UserViewProfileRequest viewRequest = new UserViewProfileRequest(token);
        Assert.assertNotNull(AUTH_ENDPOINT.profile(viewRequest).getUser());
    }

    @Test
    public void logoutUser() {
        @Nullable final String token = getUserToken();
        @NotNull final UserLogoutRequest request = new UserLogoutRequest(token);
        AUTH_ENDPOINT.logout(request);
        @NotNull final UserViewProfileRequest viewRequest = new UserViewProfileRequest(token);
        Assert.assertThrows(Exception.class, () -> AUTH_ENDPOINT.profile(viewRequest));
    }

    @Test
    public void viewProfileUser() {
        @Nullable final String token = getUserToken();
        @NotNull final UserViewProfileRequest request = new UserViewProfileRequest(token);
        @Nullable final UserDTO user = AUTH_ENDPOINT.profile(request).getUser();
        Assert.assertNotNull(user);
        Assert.assertEquals("userTestLogin", user.getLogin());
    }

}