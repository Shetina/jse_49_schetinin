package ru.t1.schetinin.tm.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLMapper;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.LinkedHashMap;
import java.util.Map;

public final class LoggerService {

    @NotNull
    private final ObjectMapper objectMapper = new YAMLMapper();

    @SneakyThrows
    public void log(final String text) {
        @NotNull final Map<String, Object> event = objectMapper.readValue(text, LinkedHashMap.class);
        @NotNull final String table = event.get("table").toString();
        @NotNull final byte[] bytes = text.getBytes();
        @NotNull final String fileName = table + ".yaml";
        final File file = new File(fileName);
        if (!file.exists()) file.createNewFile();
        Files.write(Paths.get(fileName), bytes, StandardOpenOption.APPEND);
    }

}