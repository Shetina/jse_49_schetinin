package ru.t1.schetinin.tm.test.migration;

import liquibase.Liquibase;
import liquibase.exception.LiquibaseException;
import org.junit.Test;

public class ProjectSchemeTest extends AbstractSchemeTest{

    @Test
    public void projectTest() throws LiquibaseException {
        final Liquibase liquibase = liquibase("changelog/changelog-master.xml");
        liquibase.dropAll();
        liquibase.update("project");
    }

}