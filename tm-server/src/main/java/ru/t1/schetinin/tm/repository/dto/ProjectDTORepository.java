package ru.t1.schetinin.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import ru.t1.schetinin.tm.api.repository.dto.IProjectDTORepository;
import ru.t1.schetinin.tm.dto.model.ProjectDTO;

import javax.persistence.EntityManager;

public class ProjectDTORepository extends AbstractUserOwnedDTORepository<ProjectDTO> implements IProjectDTORepository {

    public ProjectDTORepository(@NotNull final EntityManager entityManager) {
        super(entityManager);
    }

    @NotNull
    @Override
    public Class<ProjectDTO> getEntity() {
        return ProjectDTO.class;
    }

    @NotNull
    @Override
    public ProjectDTO create(@NotNull final String userId, @NotNull final String name, @NotNull final String description) throws Exception {
        @NotNull final ProjectDTO project = new ProjectDTO(name, description);
        return add(userId, project);
    }

    @NotNull
    @Override
    public ProjectDTO create(@NotNull final String userId, @NotNull final String name) throws Exception {
        @NotNull final ProjectDTO project = new ProjectDTO(name);
        return add(userId, project);
    }

}